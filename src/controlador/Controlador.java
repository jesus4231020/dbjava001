/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

/**
 *
 * @author gilpr
 */

import modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;
import vista.jifproductos;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Controlador implements ActionListener{
    private dbProducto db ;
    private jifproductos vista ;
    private boolean esActualizar;
    private int idproducto;
    
    // contructor

    public Controlador(dbProducto db, jifproductos vista) {
        this.db = db;
        this.vista = vista;
        
        // publicar el evento clic de los siguientes botones
        vista.btnbuscar.addActionListener(this);
        vista.btncancelar.addActionListener(this);
        vista.btncerrar.addActionListener(this);
        vista.btndeshabilitar.addActionListener(this);
        vista.btnguardar.addActionListener(this);
        vista.btnlimpiar.addActionListener(this);
        vista.btnnuevo.addActionListener(this);
        
    }
    // metodos helpers que son los metodos helpers 
    public void iniciarvista(){
        vista.setTitle(":: productos ::");
        vista.setVisible(true);
        vista.resize(750,750);
       try{
    this.ActualizarTabla(db.lista());
    }catch(Exception e){
    JOptionPane.showMessageDialog(vista, "surgio un error" + e.getMessage());


    }
    }
    
    
    
    public String convertirAñoMesDia(Date fecha){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }
    
    public void convertirAñoMesDia(String fecha){
        try{
            
            SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.jdcfecha.setDate(date);
        
        } catch(ParseException e){
            System.err.print(e.getMessage());
        }  
    
    }
    
    
    public void limpiar(){
     vista.txtcodigo.setText("");
     vista.txtnombre.setText("");
     vista.txtprecio.setText("");
     vista.jdcfecha.setDate(new Date());
    }
    
    public void cerrar(){
     int res = JOptionPane.showConfirmDialog(vista,"decea cerra el sistema",
             "productos",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
     if (res == JOptionPane.YES_OPTION){
         vista.dispose();
     }
    
    }
    public void habilitar(){
        vista.txtcodigo.setEnabled(true);
        vista.txtnombre.setEnabled(true);
        vista.txtprecio.setEnabled(true);
        vista.btnbuscar.setEnabled(true);
        vista.btnguardar.setEnabled(true);
    
    }
    
    public void deshabilitar(){
        vista.txtcodigo.setEnabled(false);
        vista.txtnombre.setEnabled(false);
        vista.txtprecio.setEnabled(false);
        vista.btnbuscar.setEnabled(false);
        vista.btndeshabilitar.setEnabled(false);
    
    }
    
    public boolean validar(){
     boolean exito = true;
        if(vista.txtcodigo.getText().equals("")
                || vista.txtnombre.getText().equals("")
                || vista.txtprecio.getText().equals(""))exito = false;
        
        return exito;
    
    
    
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource()==vista.btnlimpiar) this.limpiar();
        if(ae.getSource()==vista.btnguardar) { this.limpiar();this.deshabilitar();}
        if(ae.getSource()==vista.btncerrar) this.cerrar();
        if(ae.getSource()==vista.btnnuevo){this.limpiar();this.deshabilitar(); this.esActualizar=false;
                                            vista.txtcodigo.requestFocus();}
        if(ae.getSource()==vista.btnbuscar){
            Productos pro = new Productos();
            // Validar
            if(vista.txtcodigo.equals("")){
                JOptionPane.showMessageDialog(vista, "favor de capturar el codigo");
                vista.txtcodigo.requestFocus();
            }else{
                try{
                    pro = (Productos) db.buscar(vista.txtcodigo.getText());
                    if (pro.getIdProductos()!=0){
                    // aleluya si lo encontro

                    // mostrar infomracion

                        vista.txtnombre.setText(pro.getNombre());
                        vista.txtprecio.setText(String.valueOf(pro.getPrecio()));
                        // pendiente la fecha
                        this.convertirAñoMesDia(pro.getFecha());
                        vista.btnguardar.setEnabled(true);
                        vista.btndeshabilitar.setEnabled(true);
                        this.esActualizar=true;
                        this.idproducto=pro.getIdProductos();
                    } else JOptionPane.showMessageDialog(vista,"no se encontro el producto con codigo"+ vista.txtcodigo.getText());
                } catch(Exception e){
                    JOptionPane.showMessageDialog(vista,"surgio un error al buscar"+e.getMessage());
                }
            }
        }
        if(ae.getSource()==vista.btndeshabilitar){
            
            // objeto de la clase deshabilitar
            Productos pro = new Productos();
            pro.setIdProductos(this.idproducto);
            
            int res = JOptionPane.showConfirmDialog(vista,"desea deshabilitar el producto", "productos",
                    JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            if(res==JOptionPane.YES_OPTION){
                try {
                    db.desahabilitar(pro);
                    this.limpiar();
                    this.deshabilitar();
                    //actulizar la tabla
                 
                }catch(Exception e){
                    JOptionPane.showMessageDialog(vista, "surgio un error al deshabilitar" + e.getMessage());
                }
         
             
            }
        
        
        
        }
        
        if(ae.getSource()==vista.btnguardar){
        // objeto de la clase producto
        Productos pro = new Productos();
        if(this.validar()==true){
        // todo bien
        
        pro.setCodigo(vista.txtcodigo.getText());
        pro.setNombre(vista.txtnombre.getText());
        pro.setPrecio(Float.parseFloat(vista.txtprecio.getText()));
        pro.setFecha(this.convertirAñoMesDia(vista.jdcfecha.getDate()));
        
      
        try{
            if(this.esActualizar==false){
        db.insertar(pro);
        JOptionPane.showMessageDialog(vista,"Se agrego con exito el producto con codigo" + vista.txtcodigo);
                    this.limpiar();
                    this.deshabilitar();
                    //actualizar tabla
         } else {
                // se actualizara
                pro.setIdProductos(idproducto);
                db.actualizar(pro);
                JOptionPane.showMessageDialog(vista,"Se actualizo con exito el producto con codigo" + vista.txtcodigo);
                    this.limpiar();
                    this.deshabilitar();
            }
            
        } catch (Exception e) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al insertar Productos" + e.getMessage());
                }
        
        } else JOptionPane.showMessageDialog(vista, "falto informacion");
        

        
        }
    
     }
    
    public void ActualizarTabla(ArrayList<Productos> arr) {
    String[] campos = {"idproductos", "codigo", "nombre", "precio", "fecha"};

    String[][] datos = new String[arr.size()][5];
    int reglon = 0;
    for (Productos registro : arr) {
        datos[reglon][0] = String.valueOf(registro.getIdProductos());  // Corregido: getidProductos() -> getIdProductos()
        datos[reglon][1] = registro.getCodigo();  // Corregido: getcodigo() -> getCodigo()
        datos[reglon][2] = registro.getNombre();  // Corregido: getnombre() -> getNombre()
        datos[reglon][3] = String.valueOf(registro.getPrecio());  // Asumiendo que hay un método getPrecio() para obtener el precio
        datos[reglon][4] = registro.getFecha();  // Corregido: getfecha() -> getFecha()
        reglon++;
    }

    DefaultTableModel tb = new DefaultTableModel(datos, campos);
    vista.tbproductos.setModel(tb);  // Corregido: setmodel() -> setModel()
}
      
}
