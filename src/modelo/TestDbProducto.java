/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package modelo;
import javax.swing.JOptionPane;
/**
 *
 * @author gil
 */
public class TestDbProducto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        dbProducto db = new dbProducto();
        Productos pro = new Productos();
        
        //test SiExiste
        
        try {
            if(db.siExiste(1)) JOptionPane.showMessageDialog(null, "Sí existe");
            else JOptionPane.showMessageDialog(null, "No existe");
            } catch(Exception e) {
                    System.out.println("Surgió un error " + e.getMessage());
                    }
        
        // Buscar
        
        try {
            pro = (Productos) db.buscar("9");
            if(pro.getIdProductos() == 0) JOptionPane.showMessageDialog(null, "El producto buscado no existe");
            else JOptionPane.showMessageDialog(null, "Producto " + pro.getNombre() + "Precio " + pro.getPrecio());
        } catch(Exception e) {
            System.out.println("Surgió un error " + e.getMessage());
        }
    }
 }
